package org.zjava.sample.osgi.jpa.impl;

import org.apache.felix.gogo.commands.Argument;
import org.apache.felix.gogo.commands.Command;
import org.apache.karaf.shell.console.OsgiCommandSupport;
import org.zjava.sample.osgi.jpa.DataAccess;
import org.zjava.sample.osgi.jpa.model.SampleEntity;

/**
 *
 * @author Michał Blinkiewicz
 */
@Command(scope = "jpa-sample", name = "add", description = "JPA Sample ADD Command.")
public class AddCommand extends OsgiCommandSupport {

    private DataAccess dataAccess;

    @Argument(required = true)
    private String message;

    @Override
    protected Object doExecute() {
        SampleEntity sample = new SampleEntity();
        sample.setMessage(message);
        dataAccess.persist(sample);
        return null;
    }

    public DataAccess getDataAccess() {
        return dataAccess;
    }

    public void setDataAccess(DataAccess dataAccess) {
        this.dataAccess = dataAccess;
    }

}
