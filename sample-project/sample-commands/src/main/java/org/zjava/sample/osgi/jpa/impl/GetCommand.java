package org.zjava.sample.osgi.jpa.impl;

import java.util.List;
import org.apache.felix.gogo.commands.Command;
import org.apache.karaf.shell.console.OsgiCommandSupport;
import org.zjava.sample.osgi.jpa.DataAccess;
import org.zjava.sample.osgi.jpa.model.SampleEntity;

/**
 *
 * @author Michał Blinkiewicz
 */
@Command(scope = "jpa-sample", name = "get", description = "JPA Sample GET Command.")
public class GetCommand extends OsgiCommandSupport {

    private DataAccess dataAccess;

    @Override
    protected Object doExecute() {
        List<SampleEntity> samples = dataAccess.get(SampleEntity.class);
        for (SampleEntity s : samples) {
            System.out.println("Sample: " + s.getMessage());
        }
        System.out.println("--- Samples count: " + samples.size());
        return null;
    }

    public DataAccess getDataAccess() {
        return dataAccess;
    }

    public void setDataAccess(DataAccess dataAccess) {
        this.dataAccess = dataAccess;
    }

}
