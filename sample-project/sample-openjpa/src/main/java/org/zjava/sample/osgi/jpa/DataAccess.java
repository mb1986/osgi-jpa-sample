package org.zjava.sample.osgi.jpa;

import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Michał Blinkiewicz
 */
public class DataAccess {

    private EntityManager em;

    public DataAccess() {
        this.em = null;
    }

    public void persist(Object... objs) {
        for (Object obj : objs) {
            em.persist(obj);
        }
    }

    public <T> List<T> get(Class<T> clazz) {
        return em.createQuery("select s from Sample s", clazz).getResultList();
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    public EntityManager getEntityManager() {
        return em;
    }

}
